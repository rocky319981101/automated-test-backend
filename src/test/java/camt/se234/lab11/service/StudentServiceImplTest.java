package camt.se234.lab11.service;

import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.dao.StudentDaoImpl;
import camt.se234.lab11.dao.StudentNewDao;
import camt.se234.lab11.dao.StudentNewDaolmpl;
import camt.se234.lab11.entity.Student;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StudentServiceImplTest {
    StudentDao studentDao;
    StudentServiceImpl studentService;
    @Test
    public void  testFindById(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));
        assertThat(studentService.findStudentById("2"),is(new Student("2","B","BBB",3.33)));
        assertThat(studentService.findStudentById("3"),is(new Student("3","C","CCC",4.00)));
        assertThat(studentService.findStudentById("4"),is(new Student("4","D","DDD",1.25)));
        assertThat(studentService.findStudentById("5"),is(new Student("5","E","EEE",2.65)));
    }
    @Test
    public void testWithMock(){
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
       // StudentServiceImpl studentService =new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));

    }
    @Test
    public void findStudentById(){
        StudentNewDaolmpl studentNewDaolmpl = new StudentNewDaolmpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentNewDaolmpl);
        assertThat(studentService.findStudentById("5"),is(new Student("5", "A", "temp", 2.13)));
        assertThat(studentService.findStudentById("2"),is(new Student("2", "B", "BBB", 3.33)));
        assertThat(studentService.findStudentById("3"),is(new Student("3", "C", "CCC", 4.00)));
        assertThat(studentService.findStudentById("4"),is(new Student("4", "D", "DDD", 1.25)));
        assertThat(studentService.findStudentById("6"),is(new Student("6", "E", "EEE", 2.65)));
    }
    @Test
    public void testFindByPartOfId(){
        StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("22"),hasItem(new Student("223","C","temp",2.33)));
        assertThat(studentService.findStudentByPartOfId("22"),hasItems(new Student("223","C","temp",2.33),new Student("224","D","temp",2.33)));

    }
    @Test
    public void findStudentByPartOfIdd(){
        StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("22"),hasItem(new Student("223","C","temp",2.33)));
        assertThat(studentService.findStudentByPartOfId("22"),hasItems(new Student("223","C","temp",2.33),new Student("224","D","temp",2.33)));

    }
    @Before
    public void setup() {
        studentDao = mock(StudentDao.class);
        studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
    }
}
