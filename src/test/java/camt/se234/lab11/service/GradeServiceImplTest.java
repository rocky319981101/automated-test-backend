package camt.se234.lab11.service;


import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.dao.StudentDaoImpl;
import camt.se234.lab11.dao.StudentNewDaolmpl;
import camt.se234.lab11.entity.Student;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnitParamsRunner.class)
public class GradeServiceImplTest {
    StudentDao studentDao;
    StudentServiceImpl studentService;

    @Test
    @Parameters(method = "paramsForTestGerGradeParams")
    @TestCaseName("Test getGrade Params[{index}]:input is {0},expect \"{1}\"")
    public void testGetGradeparams(double score,String expectedGrade){
        GradeServiceImpl gradeService = new GradeServiceImpl();
        assertThat(gradeService.getGrade(score),is(expectedGrade));
    }
    public void testGetGrade(){
        GradeServiceImpl gradeService = new GradeServiceImpl();
        assertThat(gradeService.getGrade(100),is("A"));
        assertThat(gradeService.getGrade(80),is("A"));
        assertThat(gradeService.getGrade(78.9),is("B"));
        assertThat(gradeService.getGrade(75),is("B"));
        assertThat(gradeService.getGrade(74.4),is("C"));
        assertThat(gradeService.getGrade(60),is("C"));
        assertThat(gradeService.getGrade(59.4),is("D"));
        assertThat(gradeService.getGrade(33),is("D"));
        assertThat(gradeService.getGrade(32),is("F"));
        assertThat(gradeService.getGrade(0),is("F"));
    }
    public Object paramsForTestGerGradeParams(){
        return new Object[][]{
            {100,"A"},
            {77,"B"}
        };
    }
    public void testAllPossibleGrade(){
        GradeServiceImpl gradeService = new GradeServiceImpl();
        assertThat(gradeService.getGrade(100,100),is("A"));
        assertThat(gradeService.getGrade(90,90),is("A"));
        assertThat(gradeService.getGrade(80,80),is("B"));
        assertThat(gradeService.getGrade(70,70),is("C"));
        assertThat(gradeService.getGrade(60,60),is("C"));
        assertThat(gradeService.getGrade(50,50),is("D"));
        assertThat(gradeService.getGrade(30,30),is("F"));


    }
    @Test
    public void testGetAverageGpa(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageGpa(),is(2.712));
    }
    @Test

    public void testAverageGpa(){
        StudentServiceImpl studentService = new StudentServiceImpl();
        StudentNewDaolmpl studentNewDao = new StudentNewDaolmpl();
        studentService.setStudentDao(studentNewDao);
        assertThat(studentService.getAverageGpa(),is(2.672));
    }
    @Before
    public void setup() {
        studentDao = mock(StudentDao.class);
        studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
    }
    @Test(expected = NoDataException.class)
    public void testOutputException(){
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("55"),nullValue());
    }

    @Test(expected = ArithmeticException.class)
    public void testArithmeticException(){
        studentService.getAverageGpa();
    }

    

   


}
