package camt.se234.lab11.dao;

import camt.se234.lab11.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentNewDaolmpl implements StudentDao {
    List<Student> student;

    public StudentNewDaolmpl() {
        student = new ArrayList<>();
        student.add(new Student("5", "A", "temp", 2.13));
        student.add(new Student("2", "B", "BBB", 3.33));
        student.add(new Student("3", "C", "CCC", 4.00));
        student.add(new Student("4", "D", "DDD", 1.25));
        student.add(new Student("6", "E", "EEE", 2.65));
    }

    @Override
    public List<Student> findAll () {
        return this.student;
    }
}

