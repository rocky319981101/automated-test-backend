package camt.se234.lab11.dao;

import camt.se234.lab11.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {
    List<Student> students;

    public StudentDaoImpl() {
        students = new ArrayList<>();
        students.add(new Student("123", "A", "temp", 2.33));
        students.add(new Student("2", "B", "BBB", 3.33));
        students.add(new Student("3", "C", "CCC", 4.00));
        students.add(new Student("4", "D", "DDD", 1.25));
        students.add(new Student("5", "E", "EEE", 2.65));
    }

        @Override
        public List<Student> findAll () {
            return this.students;
        }
    }

